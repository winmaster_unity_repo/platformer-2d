extends "res://src/Actors/Actor.gd"

func _ready() -> void:
	set_physics_process(false)
	velocity.x = -speed.x
	
func _on_StompDetector_body_entered(body: PhysicsBody2D) -> void:
	get_tree().change_scene("res://src/Levels/LevelEnd.tscn")
