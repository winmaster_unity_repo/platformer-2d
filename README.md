# Platformer

Be a part of a great journey! From the starting block to the end, in the full moon :full_moon: night, a **Grumpy Square** must overcome lots of obstacles
to collect all his coins :moneybag: and defeat **blobby enemies**. 
Jump on the enemy to defeat it. Be carefull, not to get caught - you will die. :dizzy_face: 
To finish game, you must find a way to the **Great Brightness** - a mysterious portal on the end of the road. 
There will be lots of possibilities to die, and one way out. Good luck! :four_leaf_clover:

Steering with arrows or with WSAD keys:

* go left :arrow_left:

* go right :arrow_right:

* go down :arrow_down:

* jump :arrow_up:



## Download game

For Mac: <a href="https://gitlab.com/winmaster_unity_repo/platformer-2d/blob/master/release/game_mac.dmg">Game for Mac link</a> <br />
For Windows 64bit: <a href="https://gitlab.com/winmaster_unity_repo/platformer-2d/blob/master/release/game_windows.zip">Game for Windows link</a>

## Screens

![](screens/screen1.png)
![](screens/screen2.png)
![](screens/screen3.png)
![](screens/screen4.png)
![](screens/screen5.png)